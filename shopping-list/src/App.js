import {ShoppingListPage} from "./ShoppingListPage";
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import { DetailsPage } from "./details";

function App() {
    return (
        <BrowserRouter>
        <Routes>
            <Route path="/" element={<ShoppingListPage/>} />
            <Route path="/details/:itemName" element={<DetailsPage/>} />
        </Routes>
        </BrowserRouter>
    );
}

export default App;
