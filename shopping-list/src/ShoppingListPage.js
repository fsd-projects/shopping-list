import styled from 'styled-components'
import eggs from './eggs.jpg'
import milk from './milk.jpeg'
import rice from './rice.jpeg'
import hamburger from './hamburger.jpg'
import detailedEggs from './detailedEggs.jpg'
import detailedMilk from './detailedMilk.jpg'
import detailedRice from './detailedRice.jpg'
import detailedHamburger from './detailedHamburger.jpg'
import { useNavigate } from 'react-router-dom'

export const shoppingItems = [{
    'name': 'eggs',
    'price': '13.13',
    'picture': eggs,
    'store': 'shufersal',
    'description': 'very good eggs',
    'detailedPicture': detailedEggs
}, {
    'name': 'milk',
    'price': '10',
    'picture': milk,
    'store': 'rami levi',
    'description': 'best milk in town',
    'detailedPicture': detailedMilk
}, {
    'name': 'rice',
    'price': '6.90',
    'picture': rice,
    'store': 'politzer',
    'description': 'basmatic rice',
    'detailedPicture': detailedRice
}, {
    'name': 'hamburger',
    'price': '33',
    'picture': hamburger,
    'store': 'keshet teamim',
    'description': 'homemade burger',
    'detailedPicture': detailedHamburger
}];

export function ShoppingListPage() {
    const navigate = useNavigate();

    const showDetails = (item) => {
        navigate('/details/'+item.name, item.name);
    };

    return (
        <ShoppingList>
            {shoppingItems.map(item =>
                <Item onClick={() => showDetails(item)}>
                    <MainInfo>
                        <div>{item.name}</div>
                        <div>{item.price}</div>
                    </MainInfo>
                    <MainPicture>
                        <Image src={item.picture} alt="Avatar"/>
                    </MainPicture>
                </Item>
            )}
        </ShoppingList>
    );
}

const Image = styled.img`
    height: 15vmin;
`;

const MainPicture = styled.div`
    margin: 1em 1em 1em 1em;
`;
const MainInfo = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    width: 100%;
    margin: 1em 0 1em 2em;
    font-family: arial;

`;

const ShoppingList = styled.div`
    margin: 5em 20em 5em 20em;
`;
const Item = styled.div`
    margin: 5em 0 0 0;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    display: flex;
    
    &:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
`;

