import styled from "styled-components";
import { useParams } from "react-router-dom";
import { shoppingItems } from "./ShoppingListPage";

export function DetailsPage() {
    const { itemName } = useParams();
    const findItemBy = (itemName) => shoppingItems.find(item => item.name === itemName);    
    const item = findItemBy(itemName);

    if (item) {
        return (
            <Details>
                <div>{item.name}</div>
                <div>{item.store}</div>
                <div>{item.description}</div>
                <Image src={item.detailedPicture} alt="Avatar"/>
            </Details>
        );
    }
}

const Details = styled.div`
    margin: 5em 20em 5em 20em;
    font-family: arial;
`;

const Image = styled.img`
    height: 15vmin;
`;